# playnoob-prototype-sdk

Download the package [Playnoob Prototype SDK](https://gitlab.com/tanvir8955/playnoob-prototype-sdk/-/blob/main/playnoob-prototype-sdk.unitypackage)

Open an Init Scene and place the **Level Loader** prefab found inside the Prefabs folder. Enable the `Is Init Scene` bool from editor if not already enabled. Open the **LevelLoader.cs** script and change this line to indicate how many inistial non repeatable levels in your game, it can not be 0. 

`int initialLevels = 1;`

Open any scene and delete or disable any canvas and `EventSystem` and place the **PlaynoobProtoCanvas** prefab found inside the **PlaynoobPrefabs** folder. There is already a set up to facilitate the basic need. There are options inside each child Panel about how you want to show the panel. Depending on the game you may need to adjust them. 



To activate the Game Over Canvas you can just call the following methods depending on the state of the game - 

        GameOverPanelControl.Instance.EnablePanel(GameState.LevelFailed, 1.5f);
        
        GameOverPanelControl.Instance.EnablePanel(GameState.LevelCompleted, 1.5f);


where the float value is the delay to show the panel. 

**Voodoo Game Test Setup**

If the game is gonna test with Voodoo, import their latest TinySauce Unity Package and follow the exact integration process described there. 
> If the game is tested for Android fill the same respective keys in place of iOS specific keys, otherwise it will give error on iOS build

Open the **GameplayPanelControl.cs** script and inside the **GameStarted()** method enable the code blocks that better suits your game 

**On Game Start**
****
            
            //TODO: If Game does not have any levels enable the following code block
            //TinySauce.OnGameStarted();

            //TODO: If Game has levels enable the following code block
            //TinySauce.OnGameStarted(levelNumber: PlayerPrefs.GetInt(Constants.CurrentLevel).ToString());`



Open the **GameOverPanelControl.cs** script and inside the **EnablePanel()** method enable the code blocks that better suits your game

**On Level Complete** 
****

            //TODO: If Game does not have any levels enable the following code block
            //TinySauce.OnGameFinished(0); // Score = 0

            //TODO: If Game has levels enable the following code block
            //TinySauce.OnGameFinished(true, 0, levelNumber: PlayerPrefs.GetInt(Constants.CurrentLevel).ToString()); //Score = 0`


**On Level Failed**
****

            //TODO: If Game does not have any levels enable the following code block
            //TinySauce.OnGameFinished(0); // Score = 0

            //TODO: If Game has levels enable the following code block
            //TinySauce.OnGameFinished(false, 0, levelNumber: PlayerPrefs.GetInt(Constants.CurrentLevel).ToString()); //Score = 0
